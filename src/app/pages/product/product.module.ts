import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';

import { SharedModule } from '../../shared/shared.module';
import { ProductService } from './services';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgpImagePickerModule } from 'ngp-image-picker';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { ProductPageComponent } from '../product/containers';
import { ProductRoutingModule } from './product-routing.module';
import { ListComponent } from './components/list/list.component';
import { FormComponent } from './components/form/form.component';
import { MatSelectModule } from '@angular/material/select';
import { DetailComponent } from './components/detail/detail.component';
import { ConfirmComponent } from './components/confirm/confirm.component';

@NgModule({
  declarations: [
    ProductPageComponent,
    ListComponent,
    FormComponent,
    DetailComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    MatMenuModule,
    MatTableModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatFormFieldModule,
    SharedModule,
    ReactiveFormsModule,
    NgpImagePickerModule,
    InfiniteScrollModule,
    MatSelectModule,
    MatDialogModule,
    
  ],
  providers: [
    ProductService
  ]
})
export class ProductModule { }
