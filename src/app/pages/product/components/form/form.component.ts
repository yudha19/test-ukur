import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImagePickerConf } from 'ngp-image-picker';
import { Products } from '../../models';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  constructor(
    public dialogRef: MatDialogRef<FormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Products
  ) { }
  initialImage: string = '';
  imageSrc: string = '';
  config2: ImagePickerConf = {
    borderRadius: '8px',
    language: 'en',
    width: '300px',
    objectFit: 'contain',
    aspectRatio: 4 / 3,
    compressInitial: null,
  };
  name: string;
  product: string;
  price: string;
  image: string;
  description: string;

  onNoClick(): void {
    this.dialogRef.close();
  }

  onImageChanged(dataUri) {
    this.imageSrc = dataUri;
  }
  
  addProduct() {

    this.data.name = this.name;
    this.data.price = this.price ;
    this.data.image = this.imageSrc ;
    this.data.product = this.product;
    this.data.description = this.description ;

    this.dialogRef.close(this.data);

  }
}
