import { Component, Input, OnInit } from '@angular/core';
import { Products } from '../../models';
import { DetailComponent } from '../detail/detail.component';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ConfirmComponent } from '../confirm/confirm.component';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  scrollDistance = 2;
  scrollThrottle = 10;
  name: string;
  product: string;
  price: string;
  image: string;
  description: string;
  confirm: boolean = false;

  @Input() ListComponent: Products[];
  public prodData: Products[];

  constructor(public dialog: MatDialog) { }

  productDetail(data): void {
    const dialogRef = this.dialog.open(DetailComponent, {
      width: '50%',
      data: {
        name: data.name,
        product: data.product,
        price: data.price,
        image: data.image,
        description: data.description,
       }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  




  ngOnInit(): void {
    this.prodData = this.ListComponent;
  }

  onScrollDown(ev: any) {
    console.log("scrolled down!!", ev);
    this.prodData.push(...this.ListComponent);

  }
  removeProduct(element: number) {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '50%',
      data: {
        product: this.prodData[element].product,
        confirm: this.confirm,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result === true) {
        this.prodData.splice(element, 1);
      }
    });
    console.log('🚀 => ListComponent => RemoveProduct => this.prodData', this.prodData);
  }

}
