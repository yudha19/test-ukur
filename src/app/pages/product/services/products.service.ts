import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, map, take } from 'rxjs/operators';
import { Products } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public loadProductsData(): Observable<Products[]> {
    return of([
      {
        name: 'Mark Otto',
        product: 'ON the Road',
        price: '$25 224.2',
        image: './assets/box-cardboard.png',
        description: 'lorem ipsum'
      },
      {
        name: 'Jacob Thornton',
        product: 'HP Core i7',
        price: '$1 254.2',
        image: '',
        description: 'sit amet'
      },
      {
        name: 'Larry the Bird',
        product: 'Air Pro',
        price: '$1 570.0',
        image: '',
        description: ''
      },
      {
        name: 'Joseph May',
        product: 'Version Control',
        price: '$5 224.5',
        image: '',
        description: ''
      },
      {
        name: 'Peter Horadnia ',
        product: 'Let\'s Dance',
        price: '$43 594.7',
        image: '',
        description: ''
      },
      {
        name: 'Mark Otto',
        product: 'ON the Road',
        price: '$25 224.2',
        image: './assets/box-cardboard.png',
        description: ''
      },
      {
        name: 'Jacob Thornton',
        product: 'HP Core i7',
        price: '$1 254.2',
        image: '',
        description: ''
      },
      {
        name: 'Larry the Bird',
        product: 'Air Pro',
        price: '$1 570.0',
        image: '',
        description: ''
      },
      {
        name: 'Joseph May',
        product: 'Version Control',
        price: '$5 224.5',
        image: '',
        description: ''
      },
      {
        name: 'Peter Horadnia',
        product: 'Let\'s Dance',
        price: '$43 594.7',
        image: '',
        description: ''
      },
      {
        name: 'Mark Otto',
        product: 'ON the Road',
        price: '$25 224.2',
        image: './assets/box-cardboard.png',
        description: ''
      },
      {
        name: 'Jacob Thornton',
        product: 'HP Core i7',
        price: '$1 254.2',
        image: '',
        description: ''
      },
      {
        name: 'Larry the Bird',
        product: 'Air Pro',
        price: '$1 570.0',
        image: '',
        description: ''
      },
      {
        name: 'Joseph May',
        product: 'Version Control',
        price: '$5 224.5',
        image: '',
        description: ''
      },
      {
        name: 'Peter Horadnia',
        product: 'Let\'s Dance',
        price: '$43 594.7',
        image: '',
        description: ''
      },
      {
        name: 'Mark Otto',
        product: 'ON the Road',
        price: '$25 224.2',
        image: './assets/box-cardboard.png',
        description: ''
      },
      {
        name: 'Jacob Thornton',
        product: 'HP Core i7',
        price: '$1 254.2',
        image: '',
        description: ''
      },
      {
        name: 'Larry the Bird',
        product: 'Air Pro',
        price: '$1 570.0',
        image: '',
        description: ''
      },
      {
        name: 'Joseph May',
        product: 'Version Control',
        price: '$5 224.5',
        image: '',
        description: ''
      },
      {
        name: 'Peter Horadnia',
        product: 'Let\'s Dance',
        price: '$43 594.7',
        image: '',
        description: ''
      }
    ]);
  }
  
}
