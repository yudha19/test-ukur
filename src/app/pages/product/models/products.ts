export interface Products {
  name: string;
  product: string;
  price: string;
  image: string;
  description: string;
}
