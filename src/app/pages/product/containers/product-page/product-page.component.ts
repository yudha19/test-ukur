import { Component } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { ProductService } from '../../services';
import { Products } from '../../models';
import { FormComponent } from '../../components';
import { MatDialog } from '@angular/material/dialog';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent {
  public productData: Observable<Products[]>
  data : Products;
  prod = new Subject<Products>();
  constructor(
    private service: ProductService,
    public dialog: MatDialog
    ) {
    this.productData = service.loadProductsData();
  }
  name: string;
  product: string;
  price: string;
  image: string;
  description: string;
  addProduct(): void {
    const dialogRef = this.dialog.open(FormComponent, {
      width: '50%',
      data: {
        name: this.name,
        product: this.product,
        price: this.price,
        image: this.image,
        description: this.description,
      }
    });

    dialogRef.afterClosed().subscribe(result => {      
      console.log('The dialog was closed');
    });
  }
}
