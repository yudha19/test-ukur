

## How to run this project

#### 2. Run `npm install`

#### 3. Run `npm start`

#### Live demo: https://crmber-9f0df.web.app

Project ini berdasar template angular material by flatlogic, 
jadi saya sengaja tidak menghapus credit baik di komponen footer maupun di file README
hal itu sebagai bentuk saling menghargai dan mengapresiasi developer terkait.

[Demo](https://flatlogic.com/templates/angular-material-admin/demo) | [Download](https://github.com/flatlogic/angular-material-admin/archive/master.zip) | [Available versions](https://demo.flatlogic.com/angular-material-admin/) | [More templates](https://flatlogic.com/admin-dashboards) | [Support forum](https://flatlogic.com/forum)

## How can I support developers?
- Star our GitHub repo :star:
- [Tweet about it](https://twitter.com/intent/tweet?text=Amazing%20dashboard%20built%20with%20NodeJS,%20React%20and%20Bootstrap!&url=https://github.com/flatlogic/sing-app&via=flatlogic).
- Create pull requests, submit bugs, suggest new features or documentation updates :wrench:
- Follow [@flatlogic on Twitter](https://twitter.com/flatlogic).
- Subscribe to Flatlogic newsletter at [flatlogic.com](https://flatlogic.com/)
- Like our page on [Facebook](https://www.facebook.com/flatlogic/) :thumbsup:

## More from Flatlogic
- [Awesome Bootstrap Checkboxes & Radios](https://github.com/flatlogic/awesome-bootstrap-checkbox) - ✔️Pure css way to make inputs look prettier
- [React Native Starter](https://github.com/flatlogic/react-native-starter) - 🚀 A powerful react native starter template that bootstraps development of your mobile application

## Premium themes
Looking for premium themes and templates? Check out our other templates at [flatlogic.com](https://flatlogic.com/templates).

## License
The source code for the template is licensed under the MIT license, which you can find in
the LICENSE.txt file.
